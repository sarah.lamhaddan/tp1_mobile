package com.licence.tp1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class AnimalActivity extends AppCompatActivity {

    TextView nomAnimal,gestation,pnaissance,padulte,esperance;
    ImageView imgAnimal;
    EditText conservation;
    Button sauvegarde;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal);

    //get intent and create animal
        Intent intent = getIntent();
        String clikedAnimal = intent.getStringExtra("test");
        final Animal heja = AnimalList.getAnimal(clikedAnimal);

        //Image
        imgAnimal = (ImageView) findViewById(R.id.imageView);
        Resources res = getResources();
        String mDrawableName = heja.getImgFile();
        int resID = res.getIdentifier(mDrawableName , "drawable", getPackageName());
        Drawable drawable = res.getDrawable(resID );
        imgAnimal.setImageDrawable(drawable );



        //Text
        nomAnimal = (TextView) findViewById(R.id.nomAnimal);
        gestation = (TextView) findViewById(R.id.id_gestation);
        conservation = (EditText) findViewById(R.id.id_conservation);
        pnaissance = (TextView) findViewById(R.id.id_pnaissance);
        padulte = (TextView) findViewById(R.id.id_padulte);
        esperance = (TextView) findViewById(R.id.id_espvie);


        nomAnimal.setText(clikedAnimal);
        gestation.setText(heja.getStrGestationPeriod());
        conservation.setText(heja.getConservationStatus());
        pnaissance.setText(heja.getStrBirthWeight());
        esperance.setText(heja.getStrHightestLifespan());
        padulte.setText(heja.getStrAdultWeight());



        //Button Sauvegarde
        sauvegarde = findViewById(R.id.id_save);
        sauvegarde.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String modifConservation = conservation.getText().toString();
                heja.setConservationStatus(modifConservation);
                finish();


            }
        });




    }
}
