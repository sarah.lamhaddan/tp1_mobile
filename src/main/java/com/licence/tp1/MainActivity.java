package com.licence.tp1;

import com.licence.tp1.AnimalList;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayList<Animal> maList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

         maList = new ArrayList<>();

        ListView l = findViewById(R.id.ListAnimal);

        ArrayAdapter adapt= new ArrayAdapter(this, android.R.layout.simple_list_item_1,AnimalList.getNameArray());
        l.setAdapter(adapt);

        l.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                final String item = (String) parent.getItemAtPosition(position);
                Select(item);
            }
        });
    }

    private void Select(String item)
    {
        Intent intent = new Intent(getApplicationContext(),AnimalActivity.class);
        intent.putExtra("test",item);
        startActivity(intent);


    }



}

