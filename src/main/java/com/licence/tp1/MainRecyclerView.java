package com.licence.tp1;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class MainRecyclerView extends AppCompatActivity {

    private static final String[] items = AnimalList.getNameArray();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_recycler_view);

        final RecyclerView rv = (RecyclerView) findViewById(R.id.ListRecycler);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        rv.setAdapter(new MonAdapter());

    }
    class MonAdapter extends RecyclerView.Adapter<RowHolder> {

        @NonNull
        @Override
        public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return(new RowHolder(getLayoutInflater().inflate(R.layout.ligne, parent, false)));
        }

        @Override
        public void onBindViewHolder(@NonNull RowHolder holder, int position) {
            holder.bindModel(items[position]);

        }

        @Override
        public int getItemCount() {
            return(items.length);
        }
    }
    class RowHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView label;
        ImageView icon;

        public RowHolder(View row) {
            super(row);
            row.setOnClickListener(this);
            label=(TextView)row.findViewById(R.id.nomAnimal);
            icon=(ImageView)row.findViewById(R.id.imgAnimal);
        }

        @Override
        public void onClick(View v) {


            final String animalText = label.getText().toString();
            Intent intent = new Intent(MainRecyclerView.this,AnimalActivity.class);
            intent.putExtra("test",animalText);
            startActivity(intent);


        }

        void bindModel(String item) {

            label.setText(item);
            Animal heja = AnimalList.getAnimal(item);
            Resources res = getResources();
            String mDrawableName = heja.getImgFile();
            int resID = res.getIdentifier(mDrawableName , "drawable", getPackageName());
            Drawable drawable = res.getDrawable(resID );
            icon.setImageDrawable(drawable );
        }
    }
}
